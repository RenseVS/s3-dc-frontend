import apiRequest from '../ApiRequest.js'

export default async (user) => {
    const response = await apiRequest('https://localhost:5001/api/user', 'PUT', user)
    if (response.ok) {
        const result = await response.json()
        return result
    }
    return null
}