import apiRequest from '../ApiRequest.js'

export default async (userid, weapon) => {
    const response = await apiRequest('https://localhost:5001/api/weapon/'+ userid, 'PUT', weapon)
    if (response.ok) {
        const result = await response.json()
        return result
    }
    return null
}