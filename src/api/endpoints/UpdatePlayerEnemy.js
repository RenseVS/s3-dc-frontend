import apiRequest from '../ApiRequest.js'

export default async (userid, enemy) => {
    const response = await apiRequest('https://localhost:5001/api/Enemy/'+ userid, 'PUT', enemy)
    if (response.ok) {
        const result = await response.json()
        return result
    }
    return null
}