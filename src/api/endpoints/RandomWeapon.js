import apiRequest from '../ApiRequest.js'

export default async () => {
    const response = await apiRequest('https://localhost:5001/api/randomweapons', 'GET')
    if (response.ok) {
        const result = await response.json()
        return result
    }
    return null
}