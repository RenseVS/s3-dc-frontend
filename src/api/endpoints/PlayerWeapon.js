import apiRequest from '../ApiRequest.js'

export default async (userid) => {
    const response = await apiRequest('https://localhost:5001/api/weapon/' + userid, 'GET')
    if (response.ok) {
        const result = await response.json()
        return result
    }
    return null
}