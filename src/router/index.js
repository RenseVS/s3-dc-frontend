import VueRouter from 'vue-router'
import {
Home,
Game,
NewWeapon,
NewEnemy,
HealUp
} from '@/pages'

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home
    },
    {
        path: "/game",
        name: "Game",
        component: Game
    },
    {
        path: "/NewWeapon",
        name: "NewWeapon",
        component: NewWeapon
    },
    {
        path: "/NewEnemy",
        name: "NewEnemy",
        component: NewEnemy
    },
    {
        path: "/HealUp",
        name: "HealUp",
        component: HealUp
    }
];

const router = new VueRouter({
    routes: routes,
    mode: 'history'
})

export default router;