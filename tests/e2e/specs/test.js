/// <reference types='cypress'/>
describe('My First Test', () => {
  it('Visits the app root url', () => {
    cy.intercept(
      {
          method: 'GET', 
          url: 'https://localhost:5001/api/enemy/1'
      }, 
      {
          statusCode: 200,
          fixture: 'GetEnemy.json'
      }).as('getenemy')
    cy.intercept(
      {
          method: 'GET', 
          url: 'https://localhost:5001/api/user/1'
      }, 
      {
          statusCode: 200,
          fixture: 'GetUser.json'
      }).as('getuser')
    cy.visit('/game')
      cy.wait('@getuser').should(({ response }) => {
        expect(response.statusCode).to.eq(200)
    })
    cy.wait('@getenemy').should(({ response }) => {
      expect(response.statusCode).to.eq(200)
    })
  })
})
